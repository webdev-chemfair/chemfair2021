from django.shortcuts import render, redirect
from .forms import CreateUserForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url = '/authentication/signin/')
def index(request):
    return render(request, 'authentication/index.html')


def signin(request):
    if request.method == 'GET' :
        return render(request, 'authentication/signin.html')

    if request.method == 'POST' :
        username = request.POST.get('username')
        password = request.POST.get('password')

        user =  authenticate(request, username=username, password=password)

        if user is not None :
            login(request, user)
            return redirect('/authentication/')
        

        if user is None :
            messages.info(request, 'username atau password salah!')
    
    return render(request, 'authentication/signin.html')

def signup(request):
    formSignUp = CreateUserForm()

    if request.method == 'GET':
        context = {'formSignUp' : formSignUp}
        return render(request, 'authentication/signup.html', context)
    
    if request.method == 'POST':
        formSignUp = CreateUserForm(request.POST)
        if formSignUp.is_valid() :
            formSignUp.save()
            user = formSignUp.cleaned_data.get('username')
            messages.success(request, "Akun " + user + " berhasil dibuat!")
            return render(request, 'authentication/signup.html', context)
        
        return redirect('/authentication/signin/')        


@login_required
def signout(request):
    logout(request)
    return redirect('/authentication/signin/')