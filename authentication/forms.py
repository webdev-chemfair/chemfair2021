from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'email',
            'password1',
            'password2',
        ]

        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nama depan'
                }
            ),

            'last_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nama belakang'
                }
            ),

            'username': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Username'
                }
            ),

            'email': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Email'
                }
            ),

            'password1': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'password'
                }
            ),

            'password2': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'password'
                }
            ),
        }