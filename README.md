Project ChamFair2021 adalah sebuah project untuk membuat website Chemistry Fair 2021 yang akan memiliki fitur sebagai berikut :
1. Landing page berisi info-info seputar chemfair
2. Fitur login untuk autentikasi peserta
3. Halaman submisi berkas perlombaan
4. mailing list subs
5. Blog section articles


ROLE GENERAL(unauthenticated) 
- Melihat landing page
- Membaca Artikel
- Melihat About Us
- Sign Up dan Sign In

ROLE USER
- User daftar lomba/seminar (otomatis punya account)
- User yang daftar lomba (pendaftar) bakal bisa login
- Pendaftar abis login bisa liat biodata, upload bukti trf, upload berkas cp cpd (INI APA YA)
- Pendaftar bisa liat status pendaftaran dia kek udah terkonfirmasi sebagai peserta belum

ROLE ADMIN
- Bisa liat siapa aja yang udah daftar
- Akses bukti transfer sama berkas something
- Bisa ngubah status pendaftaran peserta  
